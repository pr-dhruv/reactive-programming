package com.mahii.reactive.playground;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

public class TestingFluxAndMono {

    @Test
    public void fluxTest() {

        // Once the exception is fired from Flux, the next item in the street is not going to publish,
        // so 'AfterError' is not going to publish.

        // Upper Layer with exception handling
        Flux<String> stringFlux = Flux.just("Spring", "Spring Boot", "Project")
//                .concatWith(Flux.error(new RuntimeException("Exception found")))// This is the way to attach error to Flux
                .concatWith(Flux.just("After Error"))
                .log(); // Adding Logger to see every event calls

        // Operator Layer
        stringFlux.map(data -> data.concat(" Reactor"))
                .subscribe(System.out::println, System.err::println, () -> System.out.println("Flux Completed"));    // Subscriber Layer with exception handling and onComplete() event lambda
//                .subscribe(System.out::println, error -> System.err.println(error), () -> System.out.println("Flux Completed"));    // Another way of above impl, Subscriber Layer with exception handling with onComplete event lambda

    }

    /**
     * <ul>
     * <li>StepVerifier is the class which is used to assert the Flux.</li>
     * <li>create() is method which takes flux as the input to perform tests</li>
     * <li>expectNext() is used to define the expected output.</li>
     * <li>verifyComplete() is used to perform assert operation internally.</li>
     * </ul>
     */
    @Test
    public void fluxTestWithoutError() {
        Flux<String> stringFlux = Flux.just("Spring", "Spring Boot", "Reactive Spring")
                .log();

        StepVerifier.create(stringFlux)
                .expectNext("Spring", "Spring Boot", "Reactive Spring")
                .verifyComplete();
    }

    /**
     * expectNext() can accept at least 1 parameter so we can pass all expected values.
     */
    @Test
    public void fluxTestWithoutErrorSecondWay() {
        Flux<String> stringFlux = Flux.just("Spring", "Spring Boot", "Reactive Spring")
                .log();

        StepVerifier.create(stringFlux)
                .expectNext("Spring", "Spring Boot", "Reactive Spring")
                .verifyComplete();
    }

    /**
     * expectError(Exception class) is used to expected the exception in the Flux
     * expectErrorMessage(String msg) is used to pass expected exception message
     *
     * In testing both function can't use together.
     */
    @Test
    public void fluxTestWithError() {
        Flux<String> stringFlux = Flux.just("Spring", "Spring Boot", "Reactive Spring")
                .concatWith(Flux.error(new RuntimeException("RuntimeException found")))
                .log();

        StepVerifier.create(stringFlux)
                .expectNext("Spring" )
                .expectNext("Spring Boot")
                .expectNext("Reactive Spring")
                //.expectError(RuntimeException.class)
                .expectErrorMessage("RuntimeException found")
                .verify();
    }

    @Test
    public void fluxTestWithCount() {
        Flux<String> stringFlux = Flux.just("Spring", "Spring Boot", "Reactive Spring")
                .concatWith(Flux.error(new RuntimeException("RuntimeException found")))
                .log();

        StepVerifier.create(stringFlux)
                .expectNextCount(3)
                //.expectError(RuntimeException.class)
                .expectErrorMessage("RuntimeException found")
                .verify();
    }

}
